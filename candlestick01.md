
# O que é?
Candlestick (gráficos de vela) criado no Japão, é uma técninca de análise gráfica que <br>
é mais utilizado para analisar padrões de preços de um ativo (ação, mercadoria, moeda, título).

# Por que usar?
Candlestick permite interpretar movimentos de um título dentro de um **contexto**. <br>
Fazendo a **leitura** corretamente do candlestick dentro do contexto, é capaz de prever próximos passos (tendência) do título. <br>
Com isso, auxilia a tomar uma **decisão** a partir do contexto.

# Composição do candlestick
> Construção: Preço de abertura e  do _primeiro_. Preço máximo e mínimo.

**Preço de abertura:** Preço do título em que é aberto no intervalo, <br>
é a primeira informação usada para criação do candlestick.<br>
**Preço de fechamento:** Preço do título em que é fechado no intervalo, <br>
é a última informação usada para criação do candlestick.<br>
**Preço máximo:** Maior preço do título negociado no intervalo, <br>
corresponde ao topo do pavio do candlestick.<br>
**Preço mínimo:** Menor preço do título negociado no intervalo, <br>
corresponde ao fundo do pavio do candlestick.

> ![candlestick de alta e baixa](./images/cndsk01.png)
>> **Bull/Touro vs Bear/Urso:** Dependendo do resultado do título, o mesmo representa ou alta (touro) ou baixa (urso).<br>
>> **Bull:** Resultado indica que a abertura é menor que o fechamento.<br>
>> **Bear:** Resultado indica que o fechamento é menor que a abertura.

# Ambiente de mercado
Um título ou ação é definido por estar em um dos seguintes _estados_ gerando uma tendência; <br>
- **Bull**: Tendência de alta.
- **Bear**: Tendência de baixa.
- **Incerto**: Tendência indefinida, vago.

# Padrões de candlestick
Padrões de candlestick possui a capacidade de indicar tendências predominantes do título, sendo reversão ou continuação da alta e baixa. <br>
Com essas informações à disposição, mostra quando entrar e sair de um trade ou se o trade não é interessante no momento.

# Gaps
Ocorre quando o espaço não é preenchido, não houve negociação.
> Os gaps normalmente são notícias e informações sobre algum título sendo liberado fora do horário comercial e <br> com isso o ajustamento do mercado causa se o gap.<br>

> ![Gap](./images/cndsk02.png)

# Padrão Marubozu
Marubozu é um padrão de análise gráfica com mais força, corpo longo e sua <br> característica intrínseco é que _não possui pavio_.

> ![Marubozu](./images/cndsk03.png)

- **Candle alongado:** Muito parecido com marubozu, porém a única coisa que os deferencia é o pavio, <br>
candle alongado _possui pavio_.

> ![Candle alongado](./images/cndsk04.png)

# Doji
Doji é formado quando o preço de abertura e fechamento são iguais, <br>
doji basicamente é apenas pavio, tendo um formato similar de uma cruz. <br>
Normalmente são relacionados às _viradas de mercado_  e possui algumas variações.

> ![Dojis](./images/cndsk05.png)

**Doji dragão voador**: O preço de abertura, máximo e fechamento são iguais. <br>
O comprimento do pavio pode se relacionar significamente com o futuro do preço do título, <br>
sendo quanto mais comprido, maior a força da alta. <br>
Este padrão permite estabelecer um exelente nível de suporte para compra, pois sua tendência é de alta.

> ![Doji dragão voador](./images/cndsk06.png)

> ![Dragão voador tendência alta](./images/cndsk07.png)

**Doji lápide**: O preço de fechamento, mínimo e abertura são iguais. <br>
O comprimento do pavio pode se relacionar significamente com o futuro do preço do título, <br>
sendo quanto mais comprido, maior a força da baixa. <br>
Este padrão disponibiliza um signal para venda, pois sua tendência é de baixa.

> ![Doji lápide](./images/cndsk08.png)

> ![Doji lápide tendência baixa](./images/cndsk09.png)

**Doji Pernalonga**: Considerado ser um signal de _reversão_ tanto em tendência de alta como de baixa. <br>
O comprimento do pavio é longo e o seu corpo (espessura de uma linha) é localizado relativamente no meio do candlestick.<br>
O doji pernalonga pode ser sinal de compra ou venda, é útil como signal ao aparecer em tendências.<br>

> ![Doji Pernalonga](./images/cndsk10.png)

> ![Doji Pernalonga](./images/cndsk11.png)

> ![Doji Pernalonga](./images/cndsk12.png)

**Spinning top**: Um formato parecido como um _pião_, este padrão é formado <br>
ao possuir um corpo pequeno com pavíos, sendo que o corpo encontra se mais ao centro <br>
e os pavíos meio longos. <br>
Também é um indicador em que uma _tendência_ possa começar a mudar de direção. <br>
Bastante útil em reconhecer uma _mudança_ na tendência, sendo de alta ou baixa.

> ![Spinning top](./images/cndsk13.png)

**Martelo**: Um padrão para tendência de alta, possui o corpo pequeno <br>
ao topo do candlestick e pavio longo. <br>
Interessante esperar uma _confirmação_, pois se a tendência estiver no mesmo <br>
caminho que o padrão, então realmente é um martelo.
> **Confirmação**: Uma resposta seguida de uma informação.

> ![Martelo](./images/cndsk14.png)

> ![Martelo tendência de alta](./images/cndsk15.png)

**Homem enforcado**: Semelhante ao martelo, porém é um padrão para tendência de baixa.<br>
Interessante também esperar uma _confirmação_.

> ![Homem enforcado](./images/cndsk16.png)

> ![Homem enforcado tendência de baixa](./images/cndsk17.png)

**Engolfo**: Padrão tanto para tendência de alta como de baixa. <br>
Combinação de 2 candlesticks para a formação do engolfo. <br>
Formado quando o _segundo_ candlestick, corpo e pavio cobrem <br>
o _primeiro_ candlestick e um pouco mais, <br>
fazendo com que o _engolisse_. 

> Para identificar se o engolfo está para tendência de alta ou de baixa; <br>
o _primeiro_ candlestick fornece esta informação, se o mesmo estiver com força na baixa, <br>
o _segundo_ candlestick então o _engolfa_ possuindo força na alta, e vice-versa. <br>

> ![Engolfo](./images/cndsk18.png)

> ![Engolfo de baixa](./images/cndsk19.png)

> ![Engolfo de alta](./images/cndsk20.png)

**Harami**: Conhecido também como _mulher grávida_. <br>
Combinação de 2 candlesticks para a formação do harami. <br>
Formado quando o _primeiro_ candlestick, corpo e pavio cobrem <br>
o _segundo_ candlestick e um pouco mais, <br>
assemelhando a uma mulher grávida. <br>
Lembra se do engolfo, porém o que muda é a posição dos candlestick.

> Para identificar se o harami está para tendência de alta ou de baixa; <br>
o _primeiro_ candlestick fornece esta informação, se o mesmo estiver com força na baixa, como sendo maior e _mãe_,<br>
o _segundo_ candlestick então sendo menor, como sendo a _barriga_ e possuindo força na alta. <br>
E vice-versa. <br>

> ![Harami](./images/cndsk21.png)

> ![Harami tendência](./images/cndsk22.png)

>> **Harami cross**: Variação da mulher grávida, porém a suposta barriga da mãe é um _doji_. <br>

> ![Harami cross](./images/cndsk36.png)

> ![Harami cross tendência](./images/cndsk37.png)

**Martelo invertido**: Possui as caractéristicas do martelo, porém seu corpo <br>
mantenha se no fundo. 

> Para identificar se o martelo invertido está para tendência de alta ou de baixa; <br>
Se o _primeiro_ candlestick possui força de baixa ou alta, <br>
o _segundo_ candlestick tendo as caraterísticas do martelo, então é o martelo invertido, <br>
se for de alta o martelo fica no fundo do _primeiro_, e se for de baixa, <br>
o martelo fica no topo do _primeiro_.


> ![Martelo invertido](./images/cndsk23.png)

> ![Martelo invertido tendência alta](./images/cndsk24.png)

> ![Martelo invertido tendência baixa](./images/cndsk38.png)

**Linha de encontro**: Padrão de reversão. Para identificar, <br>
o _primeiro_ e _segundo_ candlestick são longos, **de tendência oposta**. <br>
E o preço de fechamento de ambos estão a se encontrarem. <br>

> Padrão linha de encontro trabalha tanto para tendência de alta como de baixa.

> ![Linha de encontro](./images/cndsk25.png)

> ![Linha de encontro tendência](./images/cndsk26.png)

**Piercing**: Padrão _piercing line_, sinal de reversão. Forma se quando, <br>
o _primeiro_ e _segundo_ candlestick são longos, **de tendência oposta**. <br>
E o preço de abertura do _segundo_ deve ser menor que o preço mínimo do _primeiro_. <br>

> Padrão piercing trabalha tanto para tendência de alta como de baixa.

> ![Piercing line](./images/cndsk27.png)

> ![Piercing line tendência](./images/cndsk28.png)

**Linha de confiança**: Forma se quando, <br>
o _primeiro_ e _segundo_ candlestick são longos, **de tendência oposta**. <br>
Sendo que o preço de abertura do _segundo_ deve ser maior que o preço de fechamento do _primeiro_. <br>

> Padrão linha de confiança trabalha tanto para tendência de alta como de baixa.

> ![Linha de confiança](./images/cndsk29.png)

> ![Linha de confiança tendência](./images/cndsk30.png)

**Linha de separação**: Padrão que também identifica tendência, <br>
gerado quando o _primeiro_ e _segundo_ candlestick são meio longos, **de tendência oposta**, <br>
e o preço de abertura de ambos são praticamente o mesmo. <br>

> Padrão linha de separação trabalha tanto para tendência de alta como de baixa.

> ![Linha de separação](./images/cndsk31.png)

> ![Linha de separação tendência](./images/cndsk32.png)

**Linha no pescoço**: Este padrão possui 2 termos, _in neck_ e _on neck_. <br>
O _primeiro_ candlestick é longo, o _segundo_ candlestick é meio longo e **de tendência oposta**. <br>

> Padrão linha no pescoço trabalha tanto para tendência de alta como de baixa.

>**On neck**: O preço de fechamento do _segundo_ chega a ser bem próximo do preço de fechamento do _primeiro_.

> ![Linha no pescoço on neck](./images/cndsk33.png)

>>**In neck**: O preço de fechamento do _segundo_ chega a ser relativamente menor do preço de fechamento do _primeiro_.

>> ![Linha no pescoço in neck](./images/cndsk34.png)

>> ![Linha no pescoço tendência](./images/cndsk35.png)

**Three outside up**: Padrão composto por 3 candlestick. <br>
Gerado quando o _primeiro_ é uma pequena baixa, lembra se do _spinning top_. <br>
O _segundo_ já é um candlestick de alta meio longo. <br>
E o _terceiro_ fecha com seu preço de fechamento maior do que o preço máximo do _segundo_. <br>

> ![Three outside up](./images/cndsk39.png)

> ![Three outside up tendência](./images/cndsk40.png)

**Three inside up**: Padrão composto por 3 candlestick. <br>
Gerado quando o _primeiro_ é uma longa baixa. <br>
O _segundo_ já é um candlestick de alta pequeno, lembra se do _spinning top_. <br>
E o _terceiro_ fecha com seu preço de fechamento maior do que o preço de abertura do _primeiro_. <br>

> ![Three outside up](./images/cndsk41.png)

> ![Three outside up tendência](./images/cndsk42.png)

**Three white soldiers**: Padrão composto por 3 candlestick. <br>
Sendo que todos ocorrem em alta, mantendo tendência de alta. <br>

> ![Three outside up](./images/cndsk43.png)

> ![Three outside up tendência](./images/cndsk44.png)

**Morning star**: _Estrela da manhã_, padrão composto por 3 candlestick. <br>
Padrão que pode indicar reversão do mercado. <br>
Forma se quando o _primeiro_ candlesitck é de baixa. <br>
O _segundo_ é de alta, porém o tamanho chega a ser menor que o _primeiro_. <br>
E o _terceiro_ também sendo de alta, dando força para a tendência. <br>

> Estrela da manhã possui uma variação, no _segundo_ candlestick forma se um doji. <br>

> Outra variação; _bebê abandonado_, o _segundo_ candlestick fica mais distante (gap) <br>
> entre o _primeiro_ e o _terceiro_ candlestick, como se os tivessem abandonado. <br>

> ![Morning star](./images/cndsk45.png)

> ![Morning star tendência](./images/cndsk46.png)

> Padrão morning star trabalha tanto para tendência de alta como de baixa. <br>

> Variação **estrela da noite**; o _primeiro_ candlestick é de alta, <br>
> o _segundo_ e _terceiro_ de baixa. <br>

> ![Estrela da noite](./images/cndsk47.png)

> ![Estrela da noite tendência](./images/cndsk48.png)



