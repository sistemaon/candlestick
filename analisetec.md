
# Intro análise técnica

# O que é?
É uma forma de analisar, observar, estudar gráfico.

> Identificar oferta e demanda para determinar movimentos no mercado.
> Prever preço nos próximos movimentos.

**Dado técnico**: Informação do mercado sob estudo.

# Teoria de dow
Charles Henry _Dow_ foi um jornalista Americano. <br>
Fundou _Dow Jones & Company_ com Edward Jones e Charles Bergstresser. <br>

> - **Índices descontam tudo**: <br>
> Tudo que afeta a cotação dos preços das **ações** são **descontados** por esses _índices_ que relevam notícias, acidentes, resultados, eventos. <br>
> - **Mercado se move em tendência**: <br>
> Tendências podem ser de **alta** ou **baixa**. <br>
> Tendências podem ser **primárias**, **secundárias** e **terciárias**, conforme sua duração. <br>
> _Primária_: Movimento mais **longo**, duração de meses ou anos. <br>
> _Secundária_: Movimento de **correção** e **reação** do mercado, pode se retomar 1/3 a 2/3 do movimento anterior, durabilidade de semanas ou meses. <br>
> _Terciária_: Movimento de correção e reação **curtos** e duram algumas semanas. <br>
> - **Confirmação**: Necessário que dois índices devem confirmar a tendência. <br>
> - **Volume**: Ocorre um aumento significativo no volume das negociações quando o mercado **reverte** a tendência. <br>
> - **Preço de fechamento**: Cotações **máximo** e **mínimo** são **irrelevantes** perante ao cálculo de índices.
> - **A tendência é que segue**: Até que o índice se **confirme**, a tendência **antiga** é que se **mantém**.

---

> Volume é a quantidade de negociações no título durante um período. <br>
> Alteração no volume do título conforme movimento. <br>

---

# Fibonacci
Uma sequência matemática **encontrada** pelo Italiano, Leonardo Fibonacci. <br>
A sequência _fibonacci_ forma se pela _soma dos 2 últimos números da sequência_, começando do 0 e 1. <br>

> Ferramenta fibonacci auxília o _trader_ no mercado, estabelece pontos críticos. <br>
> **Retração**: _Recua_ de um ponta a outro anteriormente. <br>
> **Projeção**: _Prever_ de um ponta a outro posteriormente. <br>

---

# Ondas de elliot
Criado pelo economista Americano, Ralph Nelson Elliot. <br>
Estendeu o conhecimento da teoria de dow. <br>
Mercado ocorre em ciclos, e cada ciclo formase 5 ondas. <br>
3 ondas de impulsão (1, 3, 5) e 2 ondas de correção (2, 4). <br>
> - **Onda impulsor**: Movimento das 5 ondas (3 impulso e 2 correção). <br>
> - **Onda corretivo**: Movimento de apenas 3 ondas (ABC). <br>
>     **A**: Ocorre uma pequena baixa, correção de preço e mercado ainda aquecido para alta. <br>
>     **B**: Maior reversão de preço, retomada do mercado em alta (reaquecimento). <br>
>     **C**: Preços impulsivamente mais baixos. Aumento de volume. <br>

> - **Regras**: <br>
> Onda 2 nunca retrai mais do que 100% da onda 1, não se movimenta além do início da 1. <br>
> Onda 3 não pode ser a menor das três, nunca é a menor onda. <br>
> Onda 4 não se sobrepõe ao território de preços da onda 1, não entra na região da 1. <br>