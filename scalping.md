
###### Ter vantagem do tempo para lucrar bem entre 5 a 10 trades.

> Preste bastante atenção:
> - Tamanho dos candlestick.
> - Força na tendência.
> - Emersão de candlestick pequenos.
> - Enfraquecimento da tendência.
> - Pavios longos.
> - Esgotamento da tendência.
> - Formação de candlestick, mínima baixa, máxima alta.
> - Consolidação, virada no mercado.

---

> Evitar:
> - Falta de análise.
> - Apostas.
> - Dúvida relação a análise feita.

---

> Instrumento:
> - Stop loss.
> - Controle o tamanho da posição (volume).
> - Lucrando, assim então, pode se _aumentar_ o tamanho da posição e _trade_ pouco mais agresivamente.
> - Perdendo, assim então, pode se _reduzir_ o tamanho da posição e _trade_ pouco mais conservadamente.

---

<br>

#### Características do gráfico **Heiken Ashi** <br>

|                                |                                                   |                                                      |
| :----------------------------: | :-----------------------------------------------: | :--------------------------------------------------: |
|          **Tendência**         |                      **Alta**                     |                        **Baixa**                     |
| _Início_ da tendência          | _Subida_ de candles de _alta_                     | _Descida_ de candles de _baixa_                      |
| _Fortalecimento_ da tendência  | Candles de _alta_ mais _longos_                   | Candles de _baixa_ mais _longos_                     |
| _Enfraquecimento_ da tendência | Candles de _alta_ _menores_ (com pavio no _topo_) | Candles de _baixa_ _menores_ (com pavio no _fundo_)  |
| Consolidação / _reversão_      | _spinning-top_ / _doji_                           | _spinning-top_ / _doji_                              |

---

> Sustente: 
> - Prepare as transações **cautelosamente** após uma análise **detalhada** dos gráficos.
> - Estude, leia, acompanhe o **comportamento** antes.
> - Desenvolva uma **relação** mais **experimental** com o mercado.
> - Revise as **decisões**.
> - Seja **curioso** ao _trading_ feito ou a ser feito.
> - Tenha **consistência** e resultado.
> - **Respeitar** sinais de alerta, padrões.

---

> Gerencie:
> - Riscos.
> - Perdas, prejuízo.
> - _Stops_.
> - Banca.
> - Pressão, estresse.

---

> Foco:
> - Volatilidade.
> - Volume.
> - Comportamento.
> - Padrão.
> - Tendência.
> - Fluxo.

---

> Disciplina:
> - Agir, reagir.
> - Agilidae.
> - Paciência, calma.
> - Manter posição.
> - Observar.

---

> Cuidado:
> - Mercado lento.
> - Tédio.
> - Distração.
> - Saldo na conta (feedback).
> - Estratégia.
> - _Drawdowns_.
> - Sinais de aviso.
> - Violação da própria regra.

---

> Domina:
> - **O que** comprar.
> - **Quando** comprar.
> - **Quanto** comprar.
> - _Timing_.
> - Quando parar.
> - **Ajuste** de posição no mercado. 
